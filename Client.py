# -*- coding: utf-8 -*-
# Copyright (C) 2021 - 2022
# @author IMDEA NETWORKS
#
# This file is part of the Data Valuation Tool from the User Perspective (DVTUP) 
# module of PIMCITY.
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses>.

import json
import urllib
import requests
import time

d = {
        'transaction_id': 1,
        'model_id': 0,
        'users': [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
}

"""
data = urllib.parse.urlencode(d).encode("utf-8")
req = urllib.request.Request('http://127.0.0.1:5000/dvtup/accuracy')
req.add_header('Content-Type', 'application/json')
"""

response = requests.post('http://127.0.0.1:5000/dvtup/accuracy', json=d)

if response.status_code == 200:
    print("Accuracy for " + str(d['users']) + " in model (" + str(d['model_id']) + ") = " + str(response._content))
else:
    print("Error: " + str(response.status_code))
    print(response._content)
    
# response = requests.get('http://127.0.0.1:5000/shutdown')
# response = requests.get('http://127.0.0.1:5000/status')

d2 = {'transaction_id': 1,
      'model_id': 3,
      'users': [0,1,2,3,4,5],
      'method': 3,
      'truncation_value': 0.95,
      'stop_condition': 0.05,
      'depth': 16}


response = requests.post('http://127.0.0.1:5000/dvtup/value', json = d2)

if response.status_code == 200:
    # Gets the valuation_id
    valuation_id = int(response._content)
    # Polls the server for the valuation result every 30 secs until it
    #gets the result:
    finished = False
    while not finished:
        print ("Sleeping for 30 seconds")
        time.sleep(30)
        response = requests.get('http://127.0.0.1:5000/dvtup/value/' + str(valuation_id))
        if response.status_code == 200:
            finished = True
        else:
            print('http://127.0.0.1:5000/dvtup/value/' + str(valuation_id) + ' request returned ' + str(response.status_code) + " - '" + str(response._content) + "'")
    
    valuation = json.loads(response._content)
    
    for value in valuation:
        print("Value for user " + str(value['user_id']) + " = " + str(value['value']))
        
else:
    print('http://127.0.0.1:5000/dvtup/value/ post request returned ' + str(response.status_code) + " - '" + str(response._content) + "'")
    
