# -*- coding: utf-8 -*-
# Copyright (C) 2021 - 2022
# @author IMDEA NETWORKS
#
# This file is part of the Data Valuation Tool from the User Perspective (DVTUP) 
# module of PIMCITY.
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses>.


"""
This file starts a DVTUP server in the host machine where it is run. The port
can be configured by specifying a -p parameter followed by the port number
assigned to the server. By default it considers port 5000.

python3 DVTUPserver - starts a DVTUP server in localhost:5000

python3 DVTUPserver -p 5005 - starts a DVTUP server in localhost:5005

@author: santi
"""
import flask
from flask import request, current_app
import config # global data names
import vbdeFramework as fw # The framework
import SARIMAmodel as M # The model to be used by the framework
import TelcoDataValuation as MTelco
import ToyModels as ToyM
import pandas as pd
from datetime import datetime
import sqlite3
from markupsafe import escape
from flask import jsonify
import sys
import threading

app = flask.Flask(__name__)
app.config["DEBUG"] = True

# Connects to SQL database that stores the results of the valuations
DBcon = sqlite3.connect('DVTUP.db')
DBcursor = DBcon.cursor()
DBcursor.execute("CREATE TABLE IF NOT EXISTS Valuations (valuation_id integer PRIMARY KEY, transaction_id integer, model integer, method integer, truncation_value real, depth integer, stop_condition real, status text)")
DBcon.commit()
DBcursor.execute("CREATE TABLE IF NOT EXISTS Results (valuation_id integer, user_id text, value real, PRIMARY KEY (valuation_id, user_id))")
DBcon.commit()
DBcon.close()



# Gets the valuation ID, which is the number of records in the table Valuations
def getValuationID():
    DBcon = sqlite3.connect('DVTUP.db')
    DBcursor = DBcon.cursor()
    result = DBcursor.execute("SELECT Count(*) FROM Valuations")
    result = result.fetchall()
    DBcon.close()
    return int(result[0][0])
    


# Gets a boolean that is True if there are valuations for a certain task
def getStatus(valuation_id):
    DBcon = sqlite3.connect('DVTUP.db')
    DBcursor = DBcon.cursor()
    result = DBcursor.execute("SELECT status FROM Valuations WHERE valuation_id = " + str(valuation_id))
    result = result.fetchall()
    DBcon.close()
    if len(result) == 0:
        return "N/A"
    else: 
        return str(result[0][0])


# At the time of starting, the tool tries to retrieve the activity log folder,
#and in case there is no such activity log, it creates a new one
def logResults(results):
    file = open("DVTUP activity.log", "a")
    file.write(str(results["transaction_id"]) + "|" +
               str(results["start_time"]) + "|" +
               str(results["end_time"]) + "|" +
               str(results["model"]) + "|" +
               str(results["sources"]) + "|" +
               str(results["method"]) + "|" +
               str(results["output_status"]) + "|" +
               str(results["result"]) + "\n")
    file.close()

    

# The function returns a boolean telling whether the model_id refers to a
#valid model the server can configure
def isModelValid (model_id):
    if not isinstance(model_id, int):
        return False
    if int(model_id) < 0 or int(model_id) > 4:
        return False
    else:
        return True

# The function returns a boolean telling whether the users id in the list
#are valid i.e. they are unique identifiers of people in the P-DS. In this
#version, we assume that valid ids are integers in the closed interval [0,15]
def areUsersValid (users):
    DBconnected = False
    DBcon = None
    for user in users:    
        if isinstance(user, int):
            if user < 0 or user > 15:
                if DBconnected: DBcon.close()
                return False
        elif isinstance(user, str):
            if not DBconnected:
                DBcon = sqlite3.connect('Positions.db')
                userdata = pd.read_sql_query("SELECT User FROM tblPosition GROUP BY User;", DBcon)
                DBconnected = True
            if not user in userdata["User"].values:
                if DBconnected: DBcon.close()
                return False
        else:
            if DBconnected: DBcon.close()
            return False
    if DBconnected: DBcon.close()
    return True

# The function returns a boolean telling whether the method passed as a parameter
#is valid
def isValuationMethodValid(method):
    if not isinstance(method, int):
        return False
    if int(method) < 0 or int(method) > 5:
        return False
    else:
        return True
    
# The function returns a boolean telling whether the truncation value passed
# as a parameter is valid. It just checks that it is a float, to accomodate any
# valuation function (not only [0,1]).
def isTruncationValueValid(tvalue):
    if not isinstance(tvalue, float):
        return False
    else:
        return True
    
    
# The function returns a boolean telling whether the depth passed
# as a parameter is valid. It checks if it is an integer above 0
def isDepthValid(depth):
    if not isinstance(depth, int):
        return False
    if int(depth) <= 0:
        return False
    else:
        return True

# The function returns a boolean telling whether the stop condition passed
# as a parameter is valid. It just checks that it is a float
def isStopConditionValid(stopcondition):
    if not isinstance(stopcondition, float):
        return False
    else:
        return True

# Initializes a valuebasedDataEvaluation framework with the corresponding
# model_id.
def initializeFramework(model_id):
    # Sets up the environment depending on the model_id (already validated)
    if model_id == 0: # uses the average model
        
        c = ToyM.vbdeVoidCombiner(16) # Void combiner returns the tuple K it uses as a parameter
        m = ToyM.vbdeAvgModel() # Average Toy model
        avg = 7.5 # We set up 10 possible input sources. S_n_i = i
        v = ToyM.vbdeEuclideanSM(avg, avg) # Sets up the value function as an Euclidean similarity metric
        
        # Creates the Framework
        vbdeFW = fw.vbdeFramework(c, m, v, i_vSbuffer = False)
    elif model_id == 1: # uses the max model
        c = ToyM.vbdeVoidCombiner(16) # Void combiner returns the tuple K it uses as a parameter
        m = ToyM.vbdeMaxModel() # Average Toy model
        N = tuple(range(0,16)) # We set up 10 possible input sources. S_n_i = i
        v = ToyM.vbdeEuclideanSM(max(N), max(N)) # Sets up the value function as an Euclidean similarity metric
        
        # Creates the framework
        vbdeFW = fw.vbdeFramework(c, m, v, i_vSbuffer = False)
    elif model_id == 2: # uses the count model
        cCount = ToyM.vbdeVoidCombiner(16) # Void combiner returns the tuple K it uses as a parameter
        mCount = ToyM.vbdeSizeModel() # Average Toy model
        NCount = tuple(range(0,16)) # We set up 10 possible input sources. S_n_i = i
        vCount = ToyM.vbdeEuclideanSM(len(NCount), len(NCount))
        
        # Creates the Framework
        vbdeFW = fw.vbdeFramework(cCount, mCount, vCount, i_vSbuffer = False)
    elif model_id == 3: # uses the SARIMA prediction model
        c = M.vbPredCombiner() # Void combiner returns the tuple K it uses as a parameter
        m = M.vbSARIMAmodel() # Set up a SARIMA model
        v = M.vbPredCosSim() # Sets up the value function as an Euclidean similarity metric

        # Creates the Framework
        vbdeFW = fw.vbdeFramework(c,m,v)
    elif model_id == 4:
        c = MTelco.vbTelcoCombiner(filterYears = (2019,2020), filterMonth=(7,8), filterDays=(1,2,3,4,5,6,7), filterHours=(11,12,13,14,15,16,17))
        # Sets up a model that filters positions closer than 1 Km to the corresponding PoI
        PoI = (53.412071, -2.944376)
        m = MTelco.vbTelcoValuationModel(PoI, 2000)
        v = MTelco.vbTelcoPresence()
        vbdeFW = fw.vbdeFrameworkSubaditive(c, m, v)
    else:
        vbdeFW = None
    return vbdeFW

"""  
    In this local version of the server, all requests are valid
"""
def validateRequest(request, required_scopes):
    return True

@app.route('/status', methods=['GET'])
def hello():
    return "Hi! I'm up and running", 200



@app.route('/accuracy', methods=['POST'])
def getAccuracy():
    current_app.logger.info ("Calling getAccuracy")

    # Validates the request has authorization for the trigger:dvtup scope
    if not validateRequest(request, ['trigger:dvtup']):
        return 'Unauthorized', 405


    requestjson = request.json
    if 'transaction_id' in requestjson.keys() and 'model_id' in requestjson.keys() and 'users' in requestjson.keys():
        transaction_id = requestjson['transaction_id']
        model_id = requestjson['model_id']
        users = requestjson['users']
    else:
        return 'Invalid parameters', 402
    
    starttime = datetime.now()
    
    if not isModelValid(model_id):
        # Stores the operation in the activity log
        logResults({"transaction_id": transaction_id, 
                        "start_time": str(starttime), 
                        "end_time": "",
                        "model": model_id, 
                        "sources": str(users), 
                        "method": 99, 
                        "output_status": 400,
                        "result": ""})
        return 'Invalid model', 400
    
    for user in users:
        if not areUsersValid(users):
            # Stores the operation in the activity log
            logResults({"transaction_id": transaction_id, 
                        "start_time": str(starttime), 
                        "end_time": "",
                        "model": model_id, 
                        "sources": str(users), 
                        "method": 99, 
                        "output_status": 401,
                        "result": ""})
            return 'Invalid user', 401
        
    vbdeFW = initializeFramework(model_id)
    
        
    result = str(vbdeFW.evaluateTuple(set(users)))
    endtime = datetime.now()
    
    # Stores the operation in the activity log
    logResults({"transaction_id": transaction_id, 
                        "start_time": str(starttime), 
                        "end_time": str(endtime), 
                        "model": model_id, 
                        "sources": str(users), 
                        "method": 99, 
                        "output_status": 200, 
                        "result": result})
    return result, 200


@app.route('/value', methods=['POST'])
def triggerValuationTask():
    current_app.logger.info ("Trigger valuation task")
    # Validates the request has authorization for the trigger:dvtup scope
    if not validateRequest(request, ['trigger:dvtup']):
        return 'Unauthorized', 405

    # Get the parameters for the call
    requestjson = request.json
    if 'transaction_id' in requestjson.keys() and 'model_id' in requestjson.keys() and 'users' in requestjson.keys() and 'method' in requestjson.keys():
        transaction_id = requestjson['transaction_id']
        model_id = requestjson['model_id']
        users = requestjson['users']
        method = requestjson['method']
    else:
        return 'Invalid parameters. Either transaction_id, model_id, method or users are not part of the input json', 403
    
    starttime = datetime.now()
    
    # Initial checkings on the call params
    if not isModelValid(model_id):
        # Stores the operation in the activity log
        logResults({"transaction_id": transaction_id, 
                        "start_time": str(starttime), 
                        "end_time": "",
                        "model": model_id, 
                        "sources": str(users), 
                        "method": method, 
                        "output_status": 400,
                        "result": ""})
        return 'Invalid model', 400
    
    for user in users:
        if not areUsersValid(users):
            # Stores the operation in the activity log
            logResults({"transaction_id": transaction_id, 
                        "start_time": str(starttime), 
                        "end_time": "",
                        "model": model_id, 
                        "sources": str(users), 
                        "method": method, 
                        "output_status": 401,
                        "result": ""})
            return 'Invalid user', 401
    
    if not isValuationMethodValid(method):
        # Stores the operation in the activity log
        logResults({"transaction_id": transaction_id, 
                    "start_time": str(starttime), 
                    "end_time": "",
                    "model": model_id, 
                    "sources": str(users), 
                    "method": method, 
                    "output_status": 402,
                    "result": ""})
        return 'Invalid method', 402
    else:
        method = int(method)
    
    #  Assigns default values if truncation value, depth or stop_condition are
    # not included
    if not 'truncation_value' in requestjson.keys():
        truncation_value = 1
    else:
        truncation_value = requestjson['truncation_value']
        
    if not 'stop_condition' in requestjson.keys():
        stop_condition = 0
    else:
        stop_condition = requestjson['stop_condition']
    
    if not 'depth' in requestjson.keys():
        depth = 0
    else:
        depth = requestjson['depth']
    
    # Checks validity of input data in approximations
    if method in (3,4,5):
        if not isTruncationValueValid(truncation_value):
            # Stores the operation in the activity log
            logResults({"transaction_id": transaction_id, 
                        "start_time": str(starttime), 
                        "end_time": "",
                        "model": model_id, 
                        "sources": str(users), 
                        "method": method, 
                        "output_status": 403,
                        "result": ""})
            return 'Invalid truncation value. It must be a float number.', 403
        
            truncation_value = float(requestjson['truncation_value'])
        
        if method in (3,4,5):
            if not 'depth' in requestjson.keys() or not isDepthValid(depth):
                # Stores the operation in the activity log
                logResults({"transaction_id": transaction_id, 
                            "start_time": str(starttime), 
                            "end_time": "",
                            "model": model_id, 
                            "sources": str(users), 
                            "method": method, 
                            "output_status": 403,
                            "result": ""})
                return 'Invalid depth value. It must be an integer over 0.', 403
            
            depth = int(requestjson['depth'])
        
        if method == 3:
            
            if not 'stop_condition' in requestjson.keys() or not isStopConditionValid(stop_condition):
                # Stores the operation in the activity log
                logResults({"transaction_id": transaction_id, 
                            "start_time": str(starttime), 
                            "end_time": "",
                            "model": model_id, 
                            "sources": str(users), 
                            "method": method, 
                            "output_status": 403,
                            "result": ""})
                return 'Invalid stop condition. It must be a float above 0.', 403
            
            stop_condition = float(stop_condition)
    
    # Now we have make sure everything is fine, we trigger the work and get out
    #of here
    
    # Now the valuation work will be triggered
    starttime = datetime.now()
    
    # Inserts the operation in the database
    valuation_id = getValuationID()
    DBcon = sqlite3.connect('DVTUP.db')
    DBcursor = DBcon.cursor()
    DBcursor.execute("INSERT INTO Valuations(valuation_id, transaction_id, model, method, truncation_value, depth, stop_condition, status) VALUES (?,?,?,?,?,?,?,?)", (valuation_id, transaction_id, model_id, method, truncation_value, depth, stop_condition, "Pending"))    # Initializes the framework with the corresponding model   
    DBcon.commit()
    DBcon.close()
    
    def doValuationStuff(**kwargs):
        file = open("DVTUP Valuation Task.log", "a")
        try:
            
            file.write("Starting the valuation task")
            
            valuation_id = kwargs.get('valuation_id')
            requestjson = kwargs.get('post_data')
            transaction_id = requestjson['transaction_id']
            model_id = requestjson['model_id']
            users = requestjson['users']
            method = requestjson['method']
            
            file.write("Collected data from input dictionary\n")
            file.write("Valuation_id:" + str(valuation_id) + "\n")
            file.write("transaction_id:" + str(transaction_id)+ "\n")
            file.write("model_id:" + str(model_id)+ "\n")
            file.write("users:" + str(users)+ "\n")
            file.write("method:" + str(method)+ "\n")
            
            vbdeFW = initializeFramework(model_id)
            #Triggers the corresponding valuation work
            if method == 0:
                results = vbdeFW.getIndividualValues(users)
            elif method == 1:
                results = vbdeFW.getLOOValues(users)
            elif method == 2:
                results = vbdeFW.calculateSV(users)
            elif method == 3:
                truncation_value = requestjson['truncation_value']
                depth = requestjson['depth']
                stop_condition = requestjson['stop_condition']
                results = vbdeFW.getSV_TMC(users, len(users), len(users)*depth, stop_condition, True, truncation_value)
            elif method == 4:
                truncation_value = requestjson['truncation_value']
                depth = requestjson['depth']
                perm, nExecs, results = vbdeFW.getSV_TSS(users, depth, truncation_value)
            elif method == 5:
                truncation_value = requestjson['truncation_value']
                depth = requestjson['depth']
                perm, nExecs, results = vbdeFW.getSV_TRS(users, depth, truncation_value)
            
            file.write("Successfully ended the valuation task. Results: " + str(results))
            file.write("Updating the status of the valuation in the database")
            
            # Updates the status of the valuation_id in the database
            DBcon = sqlite3.connect('DVTUP.db')
            DBcursor = DBcon.cursor()
            DBcursor.execute("UPDATE Valuations SET status = 'Finished' WHERE valuation_id = " + str(valuation_id))
            DBcon.commit()
            
            file.write("Writing results in the database")
            
            # And inserts the results into the Results table. User keys are taken from users input. the order of the results
            #provided by functions resemble the order of the inputs
            i = 0
            for index, value in results.items():
                DBcursor.execute("INSERT INTO Results (valuation_id, user_id, value) VALUES (?,?,?)", (valuation_id, users[i], float(value)))
                DBcon.commit()
                i+=1
                
            file.write("Closing")
            
            # Closes the connection
            DBcon.close()
            
            file.write("Logging results")
            endtime = datetime.now()
            
            # Once finished, it logs the results
            logResults({"transaction_id": transaction_id, 
                        "start_time": str(starttime), 
                        "end_time": str(endtime),
                        "model": model_id, 
                        "sources": str(users), 
                        "method": method, 
                        "output_status": 200,
                        "result": str(results.values)})
            
            file.close()
        except:
            file.write("An exception occurred: " + str(sys.exc_info())+ "\n")
            
            endtime = datetime.now()
            
            # Updates the status of the valuation_id in the database to error
            DBcon = sqlite3.connect('DVTUP.db')
            DBcursor = DBcon.cursor()
            DBcursor.execute("UPDATE Valuations SET status = 'Error' WHERE valuation_id = " + str(valuation_id))
            DBcon.commit()
            DBcon.close()
            file.close()
            
            # Logs the error
            logResults({"transaction_id": transaction_id, 
                        "start_time": str(starttime), 
                        "end_time": str(endtime),
                        "model": model_id, 
                        "sources": str(users), 
                        "method": method, 
                        "output_status": 403,
                        "result": "Error in the valuation procedure"})
        
    thread = threading.Thread(target = doValuationStuff, \
                              kwargs = {'valuation_id': valuation_id, \
                                        'post_data': requestjson})
    thread.start()
    
    return str(valuation_id), 200
        

@app.route('/value/<task_id>', methods=['GET'])
def getValuation(task_id):
    current_app.logger.info ("Calling getValuation")
    # Validates the request has authorization for the trigger:dvtup scope
    if not validateRequest(request, ['read:dvtup']):
        return 'Unauthorized', 405

    valuation_id = escape(task_id)
    status = getStatus(valuation_id)
    
    if status == 'Finished':
        # Returns a json object with the valuation of users in the id passed
        #as a parameter.
        DBcon = sqlite3.connect('DVTUP.db')
        DBcursor = DBcon.cursor()
        result = DBcursor.execute("SELECT user_id, value FROM Results WHERE valuation_id = " + str(valuation_id))
        valuationResults = []
        for rows in result.fetchall():
            valuationResults.append({'user_id': rows[0],
                                    'value': rows[1]})
        return jsonify(valuationResults), 200
    else:
       return status, 400

@app.route('/shutdown', methods=['GET'])
def shutdown():
    func = request.environ.get('werkzeug.server.shutdown')
    if func is None:
        raise RuntimeError('Not running with the Werkzeug Server')
    func()
    return 'Server shutting down...'


argv = sys.argv[1:]
dvtupPort = 5000
i = 0
for arg in argv:
    if arg == "-p":
        if len(argv) < i+2:
            print("port number not specified, using default port")
        else:
            try:
                dvtupPort = int(argv[i+1])
                if dvtupPort <= 0:
                    print("Invalid port number ("+ argv[i+1]+"), using default port")
                    dvtupPort = 5000
            except:
                print("Invalid port number ("+ argv[i+1]+"), using default port")
                dvtupPort = 5000            
                
print("Starting DVTUP server in 127.0.0.1:" + str(dvtupPort))            
app.run(host = "127.0.0.1", port = dvtupPort)