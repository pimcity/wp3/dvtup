# -*- coding: utf-8 -*-
# Copyright (C) 2021 - 2022
# @author IMDEA NETWORKS
#
# This file is part of the Data Valuation Tool from the User Perspective (DVTUP) 
# module of PIMCITY.
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses>.
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 13 08:44:21 2022

@author: santi
"""
import pandas as pd
import numpy as np
import os
from datetime import datetime
import sqlite3


"""
    File generator
    
     From the sample file, it creates 9 different files just changing the
    user id of the sample.json. user ids in file 'part-0000X.json' will have
    ids starting by '0X'.

df = pd.read_json('sample.json')

coord = df.at[0,'coordinates']
print(coord[0]['latitude'])
print(coord[0]['longitude'])

files = list(range(1,10))
for i in files:
    df_copy = df.copy()
    for index, row in df.iterrows():
        length = len(df.at[index, 'user'])
        df_copy.at[index, 'user'] = "0" + str(i) + df.at[index,'user'][2:]
    
    df_copy.to_json('part-0000'+str(i)+'.json', orient='records')
"""

""" 
    Result dataset has the typical structure of identified spatio-temporal 
    data points. It informs the presence of a certain users in a certain
    point of space and time:
        * Timestamp of the observation
        * User: Id of the user
        * Lon: Longitude of the position of User at Timestamp point in time
        * Lat: Latitude of the position of User at Timestamp point in time
"""
result = pd.DataFrame(columns = ['Timestamp', 'User', 'Lon', 'Lat'])

"""
     The following function adds the information in a dataframe dfToAdd
    read from a JSON file that refers to dt datetime to the dfResult DataFrame
    that is also passed as a parameter.
    
     It unrolls the information about positions in the 
"""
def unrollDataFrame(dfToAdd, dt):
    tempDf = pd.DataFrame(columns = ['Timestamp', 'User', 'Lon', 'Lat'])
    for index, row in dfToAdd.iterrows():
        for position in row['coordinates']:
            tempDf = tempDf.append({'Tstamp': str(dt), 
                                    'User': row['user'], 
                                    'Lon': position['longitude'], 
                                    'Lat': position['latitude']},
                                   ignore_index = True)
    
    return tempDf


# Connects to SQL database that stores the results of the valuations
DBcon = sqlite3.connect('Positions.db')
DBcursor = DBcon.cursor()
DBcursor.execute("CREATE TABLE IF NOT EXISTS tblPositions (TStamp text, User integer, Lon real, Lat real)")
DBcon.commit()


root_dir = os.getcwd() # Stores current directory as root_dir
os.chdir(root_dir)
month = 7   # Month is fixed
year = 2019 # Year is fixed
all_subdirs = [d for d in os.listdir('.') if os.path.isdir(d)]
# Folders in the root folder refer to data from different days
for folder in all_subdirs:
    print("Looking into folder " + folder + " ...        ")
    day = int(folder) # The name of the folder is the day of the month
    targetFolder = os.path.join(root_dir, folder)
    os.chdir(targetFolder)
    all_subsubdirs = [d for d in os.listdir('.') if os.path.isdir(d)]
    # Subfolders within a day folder refers to observation timeframes
    for subfolder in all_subsubdirs:
        print("Looking into subfolder " + folder + "\\" +subfolder + " ...        ")
        hour = int(subfolder[0:2])   # Hour is the two first chars of the sub folder
        targetSubFolder = os.path.join(targetFolder, subfolder)
        os.chdir(targetSubFolder)
        # Gets all the json files in the subfolder
        all_json_files = [f for f in os.listdir(targetSubFolder) if os.path.isfile(os.path.join(targetSubFolder, f)) and '.json' in f]
        if len(all_json_files) > 0:
            print (str(len(all_json_files)) + " JSON files found!")
        for filename in all_json_files:
            print("\r Adding file " + filename + " of folder " + str(os.path.isfile(os.path.join(targetSubFolder, filename))) + " ...        ", end="")
            # Extracts the DataFrame from the JASON file
            df = pd.read_json(filename)
            dt = datetime(year, month, day, hour)
            # Code to append to the table tblPositions
            result = unrollDataFrame(df, dt)
            result.to_sql("tblPosition", DBcon, if_exists='append')
            DBcon.commit()
            # Code to append to a DataFrame
            # result = result.append(unrollDataFrame(df, dt), ignore_index = True)
        if len(all_json_files) > 0:
            print ("Finished!")
        
os.chdir(root_dir)       

