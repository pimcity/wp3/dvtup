# -*- coding: utf-8 -*-
# Copyright (C) 2021 - 2022
# @author IMDEA NETWORKS
#
# This file is part of the Data Valuation Tool from the User Perspective (DVTUP) 
# module of PIMCITY.
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses>.

import pandas as pd

"""
    Model configuration variables
"""

# Multi seasonal - ARIMA model parameters
#  Default values are set, though they may be optimized for a certain function
# using the function getBestParams 
paramd = (0,0,0)
param_seasonald = (0,0,0,7)
paramh = (0,0,0)
param_seasonalh = (0,0,0,24)

# Number of days to calculate the prediction for
nPredictionDays = 7

# Maximum number of iterations when running the model
maxIterations = 200

# Dask client used to pararelize executions
daskClient = None

# Input dataframe including all the time series for all the sources. Colums are
# the sources in N (ni), whereas columns yS[ni] are the observed time series y 
# by ni. yS['n'] is a specific case which contains the demand for all sources.
yS = pd.DataFrame()

#  yTest specifies the time series in the prediction period that will be used
# to evaluate the outputs of the model
yTest = pd.Series()

# yTestModule is the Module of yTest. It will be used to speedup CosSim 
#calculation
yTestModule = 1

#  SSyTest is the sum of squares of yTest. It will be used to speedup 
# RelativeSSE calculation
SSyTest = 1

# Default value provided in case the training and fitting of the model yields
# an error or a NaN
defaultValue = 0

# States whether the v(S) is independent of the order or arrival of sources in
#the set S
order_independent = True