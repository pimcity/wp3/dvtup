# -*- coding: utf-8 -*-
# Copyright (C) 2021 - 2022
# @author IMDEA NETWORKS
#
# This file is part of the Data Valuation Tool from the User Perspective (DVTUP) 
# module of PIMCITY.
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses>.

"""
Created on Jan 2021

Toy models implement some toy models and valuation functions using
value-based data valuation framework library. It will be useful to show the
power of the library.

@author: santi
"""

""" 

Count model

Count model returns the number of elements in a certain tuple. It is a basic 
model that has the following properties:
    * vi = 1 for all i
    * LOOi = 1 for all i
    * v(S) is monotonic
    * v(S U {i}) - v(S) = 1
"""

import vbdeFramework as fw
import pandas as pd

""" 
    Basic Combiners

_____________________________________________________________________________

    vbdeVoidCombiner returns the tuple K as a result of the combination. It is
    useful, for instance, to count the number of contributors to a dataset
"""
class vbdeVoidCombiner (fw.vbdeDataCombination):
    # Constructor saves the total number of sources
    def __init__(self, nSources):
        self.N = nSources
    
    # Get a combined input for the set K of sources
    def getCombinedInput(self, K):
        return K

    # Gets the set of sources
    def getSources(self):
        return self.N
    


"""
    Basic Models
    
    _____________________________________________________________________________


    Size model returns the size of an input y
"""

class vbdeSizeModel (fw.vbdeModel):
    def getModelOutput(self, x):
        return len(x)
    
"""
    Sum model returns the sum of the tuple or array y
"""    
class vbdeSumModel (fw.vbdeModel):
    def getModelOutput(self, x):
        return sum(x)
    
"""
    Avg model returns the mean of the tuple or array y
""" 
class vbdeAvgModel (fw.vbdeModel):
    def getModelOutput(self, x):
        return sum(x)/len(x)

"""
    Max model returns the mean of the tuple or array y
"""
class vbdeMaxModel(fw.vbdeModel):
    def getModelOutput(self, x):
        return max(x)

"""
    Identity model returns y=x. It does nothing
"""
class vbdeIdentityModel(fw.vbdeModel):
    def getModelOutput(self, x):
        return x

"""
    General model that imports a yS file and 
"""
class vbdeGenericModel(fw.vbdeModel):
    # Initializes the model by importing the v(S) values from the ySFile
    def __init__(self, vSfile = 'vS Generic.csv'):
        self.vS = pd.Series(index = pd.Index([], dtype = object, name = 's'), 
                            name = 'v(S)')
        self.importVS(vSfile)
    
    
    # The model just returns the value of the coalition whose value is
    #passed as an input parameter.
    def getModelOutput(self, x):
        return self.vS[str(x)]
    
    # Imports vSFile and updates self.vS array
    def importVS(self, vSfile):
        vStemp = pd.read_csv(vSfile)
        vStemp.columns = ['s', 'v(S)']
        vStemp = vStemp.set_index('s')
        self.vS = self.vS.append(vStemp['v(S)'])
    
    

"""
    Basic Value Functions
    
    _____________________________________________________________________________


    vbdeEuclideanDistance returns the difference between y and yTest and
    returns 1 - (y - yTest)/maxDistance.
"""
class vbdeEuclideanSM(fw.vbdeValueFunction):
    def __init__ (self, i_yTest, i_maxDistance):
        fw.vbdeValueFunction.__init__(self, i_yTest, 0)
        self.maxDistance = i_maxDistance
    
    def evaluateOutput(self, y1):
        d = abs(y1 - self.yTest)
        if d > self.maxDistance:
            d = self.maxDistance
        return (1-d/self.maxDistance)
    
    
"""
    vbdeVoidSM returns y as an output of the evaluation v(y)
    It is used by the vbdeGenericModel, as this model is reporting the v(y)
    directly and therefore does not need a similarity metric later in the
    cascade.
"""
class vbdeVoidSM(fw.vbdeValueFunction):
    def __init__ (self):
        fw.vbdeValueFunction.__init__(self, None, 0)
    
    def evaluateOutput(self, y):
        return y
    
"""
    vbdeToyValueMetric provides a theoretical framework to test try before you
    bid models in a series of settings. It takes three arguments at the time
    of initializing:
        - N: Number of total sources considered
        - a: controls the concaveness of v(S) vs the amount of info bought
        - b: controls the relative value of sources between them. Approx the
        contribution of s_i will be b times the contribution of s_i-1
        
    It implements a value function that returns:
        v(P) = \left( \frac{\sum_{s \in P}{b^{d(s)}}} {\sum_{s \in \mathcal{S}}{b^{d(s)}}} \right) ^ a
"""
class vbdeToyValueMetric(fw.vbdeValueFunction):
    def __init__(self, i_N, i_a, i_b):
        self.a = i_a
        self.b = i_b
        self.N = i_N
        s = range(0,i_N)
        self.denominator = sum([i_b**x for x in s])
        fw.vbdeValueFunction.__init__(self, None, 0)
    
    def evaluateOutput (self, y):
        return ((sum([self.b ** x for x in y]) / self.denominator) ** self.a)        

"""
     The following code will initialize the framework to work on a "Count"
     model and call worker i processing procedure within the framework. This 
     kind of initialization procedure is used to allow multiprocessing and
     call parallel functions in the vbdeFramework.
"""    
def CountWorker_init(i):
    #try:
        # It starts from scratch so the worker must initialize
        #the whole environment first.
        # Sets up the environment
        nSources = 9
        
        c = vbdeVoidCombiner(nSources)
        m = vbdeSizeModel()
        v = vbdeEuclideanSM(nSources, nSources)
        vbdeFw = fw.vbdeFramework(c, m, v)
                
        return vbdeFw.processWorker(i)


"""
     The following code will initialize the framework to work on a "Max"
     model and call worker i processing procedure within the framework. This 
     kind of initialization procedure is used to allow multiprocessing and
     call parallel functions in the vbdeFramework.
"""    
def MaxWorker_init(i):
    #try:
        # It starts from scratch so the worker must initialize
        #the whole environment first.
        # Sets up the environment
        nSources = 9
        
        c = vbdeVoidCombiner(nSources)
        m = vbdeMaxModel()
        v = vbdeEuclideanSM(nSources - 1, nSources - 1)
        vbdeFw = fw.vbdeFramework(c, m, v)
        vbdeFw = fw.vbdeFramework(c, m, v)
                
        return vbdeFw.processWorker(i)


"""
     The following code will initialize the framework to work on a "Max"
     model and call worker i processing procedure within the framework. This 
     kind of initialization procedure is used to allow multiprocessing and
     call parallel functions in the vbdeFramework.
"""    
def AvgWorker_init(i):
    #try:
        # It starts from scratch so the worker must initialize
        #the whole environment first.
        # Sets up the environment
        nSources = 9
        
        c = vbdeVoidCombiner(nSources)
        m = vbdeMaxModel()
        N = tuple(range(0, nSources))
        avg = sum(N)/len(N)
        v = vbdeEuclideanSM(avg, avg)
        vbdeFw = fw.vbdeFramework(c, m, v)
                
        return vbdeFw.processWorker(i)


"""
    Testing the framework and toy models
"""
if __name__ == "__main__":
    """
        Initial test
    
    # Sets up the environment
    c = vbdeVoidCombiner(9)
    m = vbdeAvgModel()
    N = tuple(range(0,9))
    avg = sum(N)/len(N)
    v = vbdeEuclideanSM(avg, avg)
    vbdeFw = fw.vbdeFramework(c, m, v)
    
    SV = vbdeFw.calculateSV(tuple(range(0,9)))
    
    # Some examples
    s = (0,1,2,3)
    print("Combined input of " + str(s)+ ":" + str(c.getCombinedInput(s)))
    print("Model output of s " + str(s) + ":" + str(m.getModelOutput(c.getCombinedInput(s))))
    print("v(s):" + str(v.evaluateOutput(m.getModelOutput(c.getCombinedInput(s)))))
    
    print("Now with the framework: " + str(vbdeFw.evaluateTuple(s)))
    """

    """
        Testing vbdeToyValueMetric
    """
    v = vbdeToyValueMetric(4, 0.2, 2)
    N = tuple(range(0,4))
    for s in fw.getCoalitions(N, 1):
        print("Value of " + str(s) + " = " + str(v.evaluateOutput(s)))

    
    