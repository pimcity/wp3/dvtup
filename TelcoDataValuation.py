# -*- coding: utf-8 -*-
# Copyright (C) 2021 - 2022
# @author IMDEA NETWORKS
#
# This file is part of the Data Valuation Tool from the User Perspective (DVTUP) 
# module of PIMCITY.
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses>.

"""
    TelcoDataValuation Model

    Setting: 

    A mall wants to run a marketing campaign targeting users that spent time close to their premises in the last week. For that purpose, the company in charge of the campaign would like to get information and the consent of such an audience to be sent promotional campaigns of the retailers hosted in that mall.

    Objectives:
    Show how spatio-temporal data may be used in the context of a PIMS and using PIMCITY’s DVTUP to prioritize and value users.
    Allow advertisers’ or ad targeting companies to sort and prioritize data of PIMS’s users based on their proximity.
    
    Demonstration:
    Preprocessing of telcos spatio-temporal data to a PDS-like relational DB. 
    Implementation of a valuation model based on time users spend close to the mall. 
    Integrate the model with the DVTUP
    Build some PPA and visualizations about the footprint of users for their internal use
"""

import warnings
import config
import pandas as pd
import numpy as np
import math
import Model
import vbdeFramework as fw
import sqlite3
from geopy.distance import geodesic


# The following function returns a tuple string compatible with sql (avoids str output "(item1,)" when lists or tuples have only 1 element)
def sqlstr(x):
    if len(x) == 0:
        return "()"
    elif len(x) == 1:
        if type(x[0]) == str:
            return "('" + str(x[0]) + "')"
        else:
            return "(" + str(x[0]) + ")"
    else:
        return str(tuple(x))

    

class vbTelcoCombiner(Model.vbDataCombination):
    def __init__(self, filterYears, filterMonth, filterDays, filterHours):
        self.DBcon_ = sqlite3.connect('Positions.db')
        #self.Data_ =  pd.DataFrame(columns = ['Year', 'Month', 'Day', 'Hour', 'User', 'Lon', 'Lat'])
        userdata = pd.read_sql_query("SELECT User FROM tblPosition GROUP BY User;", self.DBcon_)
        self.Sources_ = userdata["User"]
        self.filterYears_ = filterYears 
        self.filterMonth_ = filterMonth
        self.filterDays_ = filterDays
        self.filterHours_ = filterHours

    # Get a combined input for the set K of sources
    def getCombinedInput(self, K):
        query = "SELECT * FROM tblPosition WHERE User in " + sqlstr(K) + " and Year in " + sqlstr(self.filterYears_)  + " and Month in " + sqlstr(self.filterMonth_) + " and Day in " + sqlstr(self.filterDays_) + " and Hour in " + sqlstr(self.filterHours_) + ";"
        return pd.read_sql_query(query, self.DBcon_)

    # Gets the set of sources
    def getSources(self):
        return self.Sources_

class vbTelcoValuationModel(Model.vbModel):
    # Initializes the model to detect people that reported positions close to a certain PoI
    # at the timeframe given by the information exported by the combiner.
    #  Inputs: PoI is a tuple (lat, lon)
    def __init__(self, PoI, maxdist):
        self.PoI_ = PoI
        self.maxdist_ = maxdist
    
    
    # The model calculates the prediction based on the input x (output of a vbTelcoCombiner)
    # The input is a DataFrame with a list of positions for the users in a certain tuple K
    # The dataframe contains the following fields: Year, Month, Day, Hour, User, Lon, Lat
    def getModelOutput(self, x):
        output = x
        if len(x) == 0:
            return pd.DataFrame(columns=x.columns + ["Distance"], dtype=Object)
        else:
            for index, row in x.iterrows():
                output.at[index, "Distance"] = geodesic((row['Lat'], row['Lon']), self.PoI_).meters
            output = output[output.Distance <= self.maxdist_]
            return output

class vbTelcoPresence(Model.vbValueFunction):
    # Initializes the valuation function.
    # It returns the number of user-days that users in a model output 
    def __init__ (self, i_defaultValue = 0):
        self.defaultValue = i_defaultValue
    
    # Evaluates an output from a vbTelcoValuationModel
    def evaluateOutput(self, y):
        if len(y) == 0:
            return self.defaultValue
        result = 0
        # First we group by user and day
        g1 = y.groupby(['User', 'Day'])["Distance"].count()
        #print(g1)
        g2 = g1.groupby(['User']).count()
        #print(g2)
        result += g2.sum()
        return result



        
