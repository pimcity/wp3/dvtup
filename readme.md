Data Valuation Tool from the User's Perspective
===============================================

## Introduction

The Data Valuation Tool from an End-User Perspective (DVTUP) module is meant to provide estimated valuations of end-users' data for dataset sold through the marketplace as bulk data. In particular, DVTUP will provide tools for the TE to:
1.	Provide buyers with a hint of how valuable a piece of data is for a certain type of model or even for a specific task.
2.	Calculate a fair breakdown of data transaction charges by seller, looking forward to rewarding each user proportionally to the value that each piece of data from different sellers brings to the buyer for a specific task. 

In the first case, the output will be the expected accuracy the buyer would get from a specific dataset of the marketplace. 

In the second case, the output will estimate the percentage of a transaction value that corresponds to each seller, and a log of data and results obtained to justify rewards paid to different sellers. For that purpose, different methodologies and algorithms will be designed and implemented to allow data marketplaces to calculate such breakdown in different ways, namely: 
- Functionality 1: using traditional heuristics such as data volume or the number of sources See the paper [*Try Before You Buy: A practical data purchasing algorithm for real-world data marketplaces*](https://arxiv.org/abs/2012.08874), 

- Functionality 2: using a value-based data evaluation framework to test data on the specific task the buyer is intended to use it in, and breaking the payment according to a value function (e.g., accuracy yielded by a given model or algorithm) provided by the buyer. For more information, please see the paper [*Computing the Relative Value of Spatio-Temporal Data in Wholesale and Retail Data Marketplaces*](https://arxiv.org/abs/2002.11193).

![DVTUP functionality](DVTUP_Functionality.jpg)

An introductory video of the DVTUP can be accessed [here](https://www.youtube.com/watch?v=Wj7uQWj2pO4).

# Installation

The DVTUP module is implemented in Python3, and requires that the following Python libraries are installed: 
- Numpy version 1.19.1 or above - link to Numpy installation instructions [here](https://numpy.org/install/).
- Pandas version 1.1.1 or above - see specific installation instructions [here](https://pandas.pydata.org/docs/getting_started/install.html).
- Dask version 2.25.0 or above - link to installation instructions accessible [here](https://docs.dask.org/en/stable/install.html).
- Flask version 1.1.2 or above - link to installation instructions for Flask accessible [here](https://flask.palletsprojects.com/en/2.0.x/installation/).
- Statsmodels version 0.12.1 or above -  see specific installation instructions [here](https://www.statsmodels.org/stable/install.html).
- Markupsafe version 1.1.1 or above - see specific installation instructions [here](https://pypi.org/project/MarkupSafe/).
- GeoPy version 2.2.0 or above - see specific installation instructions [here](https://pypi.org/project/geopy/)

All the aforementioned libraries are available to be installed by using pip:
```
pip install <library_name>
```
For example, the following set of commands will install the last version of the libraries using pip: 
```
pip install numpy
pip install pandas
pip install dask
pip install Flask
pip install statsmodels
pip install -U MarkupSafe
pip install geopy
```

# Usage

## Execution

To run the server, the following command must be executed from the folder where the source code is stored:
```
python DVTUPServer.py -p <PORT_NUMBER>
```

Where <PORT_NUMBER> specifies the port number the server will listen to. By default, DVTUP server will use port 5000. Consequently:

```
python DVTUPserver.py - starts a DVTUP server in localhost:5000
python DVTUPserver.py -p 5005 - starts a DVTUP server in localhost:5005
```

The server will check any dependencies from external PIMCITY modules during the initialization process, and will prompt you in case further action is needed. In this version, the module will work with preloaded models and data to show the feasibility of executing these valuations. Refer to the online documentation to test API calls to the server and use its functionality.

## Usage Examples

The DVTUP API documentation is available on an online interactive documentation website for the services it provides (See [Swagger Open API definition](https://easypims.pimcity-h2020.eu/swagger/dvtupSwagger.html)). By visiting it, the reader can quickly understand and try out the API functionalities. It includes more detail, such as JSON schemas for inputs and outputs of all endpoints and services. 

Please refer to the following Jupyter notebooks which include examples to show how to deal with the different APIs and components of the DVTUP. 
- vbde Sample Test.ipynb - shows how to deal with the value-based data valuation framework and their abstract classes, and how to calculate the relative value of data sources in different sample cases.
- DVTUP Client.ipynb shows how to start the server and use a REST HTTP client to send valuation requests and get their results.

# Changes

- *April 2021* - Initial commit intended to be used in a local environment
- *July 2021* - Modifications and adaptations for the DVTUP to be deployed on PIMCITY's development servers. Debugging minor errors in valuation functions.
- *October 2021* - Removed an error in vbdeFramework.py with the toy models that do not use the vS buffering of the framework
- *December 2021* - Integration with KeyCloak
- *April 2022* - Added the telco location data demonstration demo, including an update of the value-based data valuation framework and the following new files:
    1. Sample Telco Location Synthetic Data.zip - includes sample data to test the application
    2. Telco Data.py - used to pre-processed sample data and create the database resembling the PDS
    3. TelcoDataValuation.py - Including the models carrying out the location data valuation
    4. TelcoDataDemonstrator.ipynb - Jupyter notebook showing the functionality of the tool in a local environment.

# License

The DVTUP is distributed under AGPL-3.0-only, see the `LICENSE` file. This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses>.

Copyright (C) 2021  IMDEA Networks
