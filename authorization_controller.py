from keycloak import KeycloakOpenID

"""
controller generated to handled auth operation described at:
https://connexion.readthedocs.io/en/latest/security.html
"""


def check_OAuth2(token):

    keycloak_openid = KeycloakOpenID(
        server_url="https://easypims.pimcity-h2020.eu/identity/auth/",
        client_id="dvtup",
        realm_name="pimcity",
    )

    KEYCLOAK_PUBLIC_KEY = (
        "-----BEGIN PUBLIC KEY-----\n"
        + keycloak_openid.public_key()
        + "\n-----END PUBLIC KEY-----"
    )

    options = {
        "verify_signature": True,
        "verify_aud": True,
        "verify_exp": True,
        "audience": "dvtup",
    }

    token_info = keycloak_openid.decode_token(
        token, key=KEYCLOAK_PUBLIC_KEY, options=options
    )

    scope = token_info["scope"].split()

    return {"scopes": scope}


def validate_scope_OAuth2(required_scopes, token_scopes):
    return set(required_scopes).issubset(set(token_scopes))

"""
print("Checking token: cc57f035-d13a-47c6-a70f-dd38aa872760")
response = check_OAuth2("eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJxZ0szQTJRSEttTDdPWlhKZ0d6LVRDY2Y5OVZMVHJMVC1yeE5hOFR0RWFNIn0.eyJleHAiOjE2MzkwMzc2NzMsImlhdCI6MTYzOTAzNzM3MywianRpIjoiMTBjZTMyOGEtYTJiNy00NWM2LWI0NDItNWNjODE1ODlkNmJkIiwiaXNzIjoiaHR0cHM6Ly9lYXN5cGltcy5waW1jaXR5LWgyMDIwLmV1L2lkZW50aXR5L2F1dGgvcmVhbG1zL3BpbWNpdHkiLCJzdWIiOiIzOTg1YTQ4NC1lMjlkLTRkMjEtYjk4YS03ODlhOWUzNzJjMTQiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJkdnR1cCIsImFjciI6IjEiLCJzY29wZSI6InRyaWdnZXI6ZHZ0dXAgcmVhZDpkdnR1cCIsImNsaWVudElkIjoiZHZ0dXAiLCJjbGllbnRIb3N0IjoiMTkyLjE2OC4yMzYuMTYiLCJjbGllbnRBZGRyZXNzIjoiMTkyLjE2OC4yMzYuMTYifQ.JDX4_8yLUwzr_zSX8kHXDA5ff6KALcD19GVcJfAaBfbnTZOdI2ndu0jNgDfFaFFLzbDTpgwoPrpoky9sSP7muR-7GPcU_9MSGeYrArLJZgl3gPP_QtSJOXzdsDtRJAbsyT0Joi4jE1EsM9ixCNx9iumq9SQE0PeNNeTv3X2jvB7QXVShzpPFJ_PveGARpmNc5critvT7YiDSJTugNeahJ27AbgPztvgTs_XJ2QZZOz55MEPGrrcSdI1V5nA8qg0I5xL-Ku532MbrcgzKBF8d8bhAsefbQGCVGfKMF-GSmZ-xeP9vGL48Rou6Km7GdnineFSK-QtjO4JoB3-d6JZ-KA")
print(str(response))
print(validate_scope_OAuth2(['trigger:dvtup'], response['scopes']))
"""