# -*- coding: utf-8 -*-
# Copyright (C) 2021 - 2022
# @author IMDEA NETWORKS
#
# This file is part of the Data Valuation Tool from the User Perspective (DVTUP) 
# module of PIMCITY.
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses>.

"""
Created on Feb 2021

SARIMA prediction MODEL

Library containing the functions needed to execute the SARIMA model that is
used for prediction purposes in the model. Moreover, as it is tighly coupled
with the model, it includes also different metrics that will be used in order
to evaluate the "goodness" of the prediction.

Externally, the model will provide the following functions:
  * getBestParams(y, depth):
  * getDemandForecast(y, nd, paramd, param_seasonald, paramh, param_seasonalh,
                      maxiter = 200)

The following distance functions are included, all of them are working with
pd.Series():
  * calculateCosSim(y1, y2):
  * CalculateRelativeSSEDistance(y1, ytest)
  * calculateCrossCorrelation(y1, y2)
  * calculateNumSim(y1, y2)

@author: santi
"""

import warnings
import config
import pandas as pd
import numpy as np
import math
import statsmodels.api as sm
import itertools
import Model
import vbdeFramework as fw



class vbPredCombiner(Model.vbDataCombination):
    # Get a combined input for the set K of sources
    def getCombinedInput(self, K):
        return getCombinedInput(K)

    # Gets the set of sources
    def getSources(self):
        return getSources()


class vbSARIMAmodel(Model.vbModel):
    # Initializes the model by importing the v(S) values from the ySFile
    def __init__(self):
        initialize()
    
    
    # The model calculates the prediction based on the input x (a combination)
    def getModelOutput(self, x):
        return getModelOutput(x)

class vbPredCosSim(Model.vbValueFunction):
    """  Attributes: 
            defaultValue, which is the value which will be used in case
        of any failure either in the model fitting or in the evaluation of the
        result from the model.
            yTest, it is used the variable in SARIMA model
    """    
    def __init__ (self, i_defaultValue = 0):
        self.defaultValue = i_defaultValue
    
    def evaluateOutput(self, y1):
        return calculateCosSim(y1)




"""
    Initialize the model
    The following function is used to initialize the model. The parameters
    included for the SARIMA multiseasonal should be the result of a grid
    search analysis.    
"""
def initialize():
    initializeModel(nd = 14, maxiter = 200, \
                      pd = (0,1,1), psd = (0,1,1,7), \
                      ph = (1,0,1), psh = (1,0,1,24))
    

"""
    getCombinedInput - function used to combine data sources to a y(s)
    Input: set of sources K
    Output: Combined input for all sources in K: y(S(K)) of y(S)
    Assumptions: elements of K in config.yS columns
"""    


# Returns the demand curve for a set of sources s, by averaging all 
#contributions in yS of columns. It is an instantiation of the module to 
#combine inputs. To be changed to introduce a new class: demandCombination.
def getCombinedInput(K):
    result = [0] * config.yS.index.size
    for source in K:
        if not source in config.yS.columns:
            print('WARNING: getCombinedInput - No data for source: ' +str(source))
        else:
            result += config.yS[source]
    
    result.index.freq = 'h'
    
    return result
    
# Gets the set of sources
def getSources():
    sources = tuple(config.yS.columns)
    return sources
    


""" 
    This function decomposes an hourly demand function into two subcomponents:
    #y[n] is decomposed in a daily average (yd[n]) and a hourly percentage component (yh[n]). 
    #y[n] is the product of an upsampled yd[n] and yh[n]
    samplingPeriod2 is shorter than samplingPeriod1
    yh_input is samplingPeriod2
    Returns yavgd_output of period samplingPeriod1
    Returns yh_norm_output of period samplingPeriod2
    
    MULTIPLICATIVE DECOMPOSITION
""" 
def getModelAIC(y, param, param_seasonal):
        print("Execution of the model for " + str(param) + \
                      "x" + str(param_seasonal))
        try:
            mod = sm.tsa.statespace.SARIMAX(y,
                                            order=param,
                                            seasonal_order=param_seasonal,
                                            enforce_stationarity=False,
                                            enforce_invertibility=False)
            
            results = mod.fit(maxiter = config.maxIterations)
    
            return results.aic
        except:
            return float(math.nan)

"""
    initializeModelGridSearch
    The following lines of code allows to initialize the SARIMA model and 
    perform a parallelized grid model analysis (depth 2) to choose the best 
    params for the model and stores in the global variables in config.py. 
    It assumes that a dask multiprocess environment is already in place and
    accesible through config.daskClient. The procedure is executed in multiple
    processes to accelerate the gridSerach process. 
    
    This must be used only once in order to set up the model.
"""
def initializeModelGridSearch(nd = 7, maxiter = 200, depth = 2):
    print("Initializing SARIMA model and building best params")
    loadDemand()
    config.nPredictionDays = nd
    config.maxIterations = maxiter
    (config.paramd, config.param_seasonald, config.paramh, \
     config.param_seasonalh) = getBestParams(getCombinedInput(getSources()),
                                             depth = 2)

"""
    initialize demand
    Loads demand from demand files in the corresponding folder
"""
def loadDemand():
    print("Loading demand from files")
    # Demand Input by source in the training period
    DemandInput = pd.read_csv('yS.csv')
    DemandInput['pickup_datetime'] = pd.to_datetime(DemandInput['pickup_datetime'], errors='coerce')
    config.yS = DemandInput.set_index('pickup_datetime')
    
    # Interpolates linearly between ibservations when needed (nan)
    config.yS.interpolate(inplace = True, method = 'linear')
    config.yS.fillna(inplace = True, method = 'bfill')
    config.yS.fillna(0, inplace = True)

    # Rename columns to integers - sources will be integers
    config.yS.columns = list(range(len(config.yS.columns)))
    
    DemandInput = pd.read_csv('yTest.csv')
    DemandInput['pickup_datetime'] = pd.to_datetime(DemandInput['pickup_datetime'], errors='coerce')
    config.yTest = DemandInput.set_index('pickup_datetime')['yTest']
    
    # Configure test demand function
    config.yTestModule = calculateModule(config.yTest)
    config.SSyTest = (config.yTest*config.yTest).sum()
    
    
"""
    initializeModel
    Manual initialization of the model, once we know the right parameters to
    use.
"""
def initializeModel(pd, psd, ph, psh, nd = 7, maxiter = 200):
        print("Initializing SARIMA model with input params")
        loadDemand()
        config.nPredictionDays = nd
        config.maxIterations = maxiter
        config.paramd = pd
        config.param_seasonald = psd
        config.paramh = ph
        config.param_seasonalh = psh
        
""" 
    This function decomposes an hourly demand function into two subcomponents:
    #y[n] is decomposed in a daily sum (yd[n]) and a hourly percentage component (yh[n]). 
    #y[n] is the sum of an upsampled yd[n] and yh[n]
    samplingPeriod2 is shorter than samplingPeriod1
    yh_input is samplingPeriod2
    Returns ysumd_output of period samplingPeriod1
    Returns yh_norm_output of period samplingPeriod2
    
    MULTIPLICATIVE DECOMPOSITION
""" 
def demandDecompose(yh_input, samplingPeriod1, samplingPeriod2):
    ysumd_output = yh_input.resample(samplingPeriod1).sum()
    ysumd_h = ysumd_output.resample(samplingPeriod2).pad()
    
    # Appends 23 more hours to the last sample. CHAPUZA!!!
    i = 1
    deltat = np.timedelta64(1, samplingPeriod2)
    newT = ysumd_h.index.values[ysumd_h.size-1]
    newValue = ysumd_h[ysumd_h.size-1]
    while i <= 23:
        i += 1
        newT = newT + deltat
        ysumd_h = ysumd_h.append(pd.Series(newValue, index = [newT]))
    
    # Calculates yh_percent_output -> dividing hourly demand by the sum of that day
    yh_percent_output = pd.Series()
    for t in yh_input.index.values:
        yh_percent_output = yh_percent_output.append(pd.Series(yh_input[t]/ysumd_h[t], index = [t]))
    yh_percent_output.index.freq = 'h'
    ysumd_output.index.freq = 'd'
    return (ysumd_output, yh_percent_output)
    
""" 
    This function composes an hourly demand function from its two subcomponents:
    #a daily sum (yd[n]) and 
    #a hourly normalized percentage component (yh[n])
    #y[n] is the multiplication of an upsampled yd[n] and yh[n] and has T = upsamplingPeriod
    
    MULTIPLICATIVE COMPOSITION
""" 
def demandCompose(yd_input, yh_input, downsamplingPeriod):
    ysumd_h = yd_input.resample(downsamplingPeriod).pad()
    # Appends 23 more hours to the last sample - chapuza
    i = 1
    deltat = np.timedelta64(1, 'h')
    newT = ysumd_h.index.values[ysumd_h.size-1]
    newValue = ysumd_h[ysumd_h.size-1]
    while i <= 23:
        i += 1
        newT = newT + deltat
        ysumd_h = ysumd_h.append(pd.Series(newValue, [newT]))
    
    #Calculates y_output  -> multiplying
    y_output = pd.Series()
    for t in yh_input.index.values:
        y_output = y_output.append(pd.Series(yh_input[t]*ysumd_h[t], index = [t]))
    
    return (y_output)

    
"""
GRID SEARCH FOR THE BEST SARIMA model: (p,d,q)^s
 The function does a search of SARIMA models for different parameters to
fit the time series passed as input parameter, up to a maximum depth, which is
also an input parameter of the function. It returns the set of parameters for
the best model, considering AIC as the metric to decide which is the best 
fit. This considers not only the precision of the fitted curve but also
computational complexity of the model.
 The function works only in one thread
"""
def gridSearchSARIMA(y, stationality, depth = 2):
    
    
    # Selecting parameters for seasonal ARIMA
    # Define the p, d and q parameters to take any value between 0 and 4
    p = d = q = range(0, depth)
    
    # Generate all different combinations of p, q and q triplets
    pdq = list(itertools.product(p, d, q))
    print(pdq)
    
    # Generate all different combinations of seasonal p, q and q triplets
    seasonal_pdq = [(x[0], x[1], x[2], stationality) for x in list(itertools.product(p, d, q))]
       
    warnings.filterwarnings("ignore") # specify to ignore warning messages
    
    # Minimum AIC Score
    minAIC = 1000000
    
    # Best Params for model fitting
    bestParams=(pdq[0], seasonal_pdq[0])
    
    for param in pdq:
        for param_seasonal in seasonal_pdq:
            try:
                mod = sm.tsa.statespace.SARIMAX(y,
                                                order=param,
                                                seasonal_order=param_seasonal,
                                                enforce_stationarity=False,
                                                enforce_invertibility=False)
    
                results = mod.fit()
    
                print('ARIMA{}x{}{} - AIC:{} \nBSE:\n{}'.format(param, param_seasonal, stationality, results.aic, results.bse))
                
                if results.aic < minAIC:
                    print('New Best Model found: ARIMA{}x{}{} - AIC:{}'.format(param, param_seasonal, stationality, results.aic))
                    minAIC = results.aic
                    bestParams = (param, param_seasonal)
            except:
                continue
    return bestParams
    
"""
GRID SEARCH FOR THE BEST SARIMA model: (p,d,q)x(p,d,q)^s
 The function does a search of SARIMA models for different parameters to
fit the time series passed as input parameter, up to a maximum depth, which is
also an input parameter of the function. It returns the set of parameters for
the best model, considering AIC as the metric to decide which is the best 
fit. This considers not only the precision of the fitted curve but also
computational complexity of the model.
 The function works using multiple processes
"""    


def gridSearchSARIMAparallel(y, stationality, depth):
    print("Starting a gridSearchSARIMA parallel for s = " + \
          str(stationality) + " with depth = " + str(depth))
    
    # Selecting parameters for seasonal ARIMA
    # Define the p, d and q parameters to take any value between 0 and 4
    p = d = q = range(0, depth)
    
    # Generate all different combinations of p, q and q triplets
    pdq = list(itertools.product(p, d, q))
    #print("PDQ: " + str(pdq))
    
    # Generate all different combinations of seasonal p, q and q triplets
    seasonal_pdq = [(x[0], x[1], x[2], stationality) for x in list(itertools.product(p, d, q))]
    #print("Seasonal pdq: " + str(seasonal_pdq))
    
    # DataFrame to store the results of the grid Search
    dfAIC = pd.DataFrame(columns=['p', 'd', 'q', 'ps', 'ds', 'qs', 's', 'i'])
     
    futures = []
    
    i = 0
    
    for param in pdq:
        for param_seasonal in seasonal_pdq:
                #print("Appending future execution of " + str(param) + \
                #      "x" + str(param_seasonal))
                futures.append(config.daskClient.submit(getModelAIC, y, 
                                                  param, param_seasonal))
                dfAIC = dfAIC.append({'p' : param[0], 'd' : param[1], 
                                      'q' : param[2], 
                                      'ps' : param_seasonal[0], 
                                      'ds' : param_seasonal[1],
                                      'qs' : param_seasonal[2], 
                                      's' : param_seasonal[3],
                                      'i' : i}, ignore_index = True)
                i += 1
                
    #DEBUG
    print("Executing " + str(len(futures)) + " model fitting works")
    
    # Gets the row with minimum 'aic'
    results = config.daskClient.gather(futures)
    minAIC = min(results)
    print("Minimum AIC: " + str(minAIC))
    i = results.index(minAIC) 
    
    #DEBUG LINE
    print(dfAIC)
    
    param = (dfAIC.at[i,'p'], dfAIC.at[i, 'd'], dfAIC.at[i, 'q'])
    param_seasonal = (dfAIC.at[i, 'ps'], dfAIC.at[i, 'ds'], dfAIC.at[i,'qs'], 
                      dfAIC.at[i,'s'])
    bestParams = (param, param_seasonal)
    
    return bestParams
    
"""
    Get Best Params for the multi-seasonal models
    
    The function performs two parallel grid analysis and returns the best 
    params for the hourly and daily prediction models. It returns the params in
    a 4 element tuple: (paramd, param_seasonald, paramh, param_seasonalh)
"""
def getBestParams(y, depth):
    print("Entering getBestParams") # DEBUG
    
    # Decomposes the input time series
    (yd, yh) = demandDecompose(y, 'd', 'h')
    
    # Get best params for yd
    (paramd, param_seasonald) = gridSearchSARIMAparallel(yd, 7, depth)
    
    # Get best params for yh
    (paramh, param_seasonalh) = gridSearchSARIMAparallel(yh, 24, depth)
    
    #Returns the best params
    return (paramd, param_seasonald, paramh, param_seasonalh)
    
"""
    Train model
    
    The function trains a multiseasonal model with the input signal and 
    input parameters and returns the predicted function for a period of nd
    days after the training period expires.
    
"""
def getSingleModelFit(y, param, param_seasonal):
    mod = sm.tsa.statespace.SARIMAX(y,
                                    order=param,
                                    seasonal_order=param_seasonal,
                                    enforce_stationarity=False,
                                    enforce_invertibility=False)
        
    results = mod.fit(maxiter = config.maxIterations)

    return results
    
"""
    getModelOutput
    Provides the output of the model with the parameters stated in this
    module for the input y.

"""
def getModelOutput(y):
    # Get the number of hours for which we need a forecast
    nh = config.nPredictionDays*24
    
    # Decomposes the input series:
    (yd, yh) = demandDecompose(y, 'd', 'h')
    
    # Trains the single models
    resultsd = getSingleModelFit(yd, config.paramd, config.param_seasonald)
    resultsh = getSingleModelFit(yh, config.paramh, config.param_seasonalh)
    
    # And returns the reconstructed forecast function
    return demandCompose(resultsd.forecast(config.nPredictionDays), \
                              resultsh.forecast(nh), 'h')
    
"""
   Prediction valuation functions include:
       * Cross correlation
       * Numerical similarity
       * Cosine similarity
       * - relative sum of squared errors
"""
def calculateModule(y):
    return math.sqrt((y*y).sum())


def calculateCosSim(y):
    return (y*config.yTest).sum() / calculateModule(y) / config.yTestModule


# Calculates Pearson's Correlation of signals y1, y2 without any shifting     
def calculateCrossCorrelation(y):
    avgy1 = y.mean()
    avgy2 = config.yTest.mean()
    y1norm = y - avgy1
    y2norm = config.yTest - avgy2
    result = (y1norm*y2norm).sum() / \
        math.sqrt(((y1norm*y1norm).sum()*(y2norm*y2norm).sum()))
    return result

    
# Calculates numerical similarity of signals y1 and y2, which is calculated as the
#average similarity for all the points in the distribution.
# Takes Pandas Series as inputs
def calculateNumSim(y):
    absErr = (y - config.yTest).abs()
    sumVal = y.abs() + config.yTest.abs()
    return 1 - (absErr/sumVal).mean()

    
# Gets the square sum of the errors of two arrays
def calculateSSE(y1, y2):
    return ((y1-y2)**2).sum()
    

# Gets - the relative sum of the square of errors
def calculateRelativeSSE (y):
    return -calculateSSE(y, config.yTest)/config.SSyTest

"""
    WorkerProcess_init initializes a SARIMA model and start calculating the
    value of tuples included as an input in their corresponding file.
"""
def WorkerProcess_init(i):
    try:
        # It starts from scratch so the worker must initialize
        #the whole environment first.
        c = vbPredCombiner() # Void combiner returns the tuple K it uses as a parameter
        m = vbSARIMAmodel() # Set up a SARIMA model
        v = vbPredCosSim() # Sets up the value function as an Euclidean similarity metric

        # Creates the Framework
        vbdeSARIMA = fw.vbdeFramework(c, m, v)
        
        # Retrieve the schedule of tasks to perform:
        taskImport = (pd.read_csv('worker '+str(i)+' schedule.csv'))[str(i)]
        # Retrieve existing vS file to avoid duplication of works
        #importVSFile() # TO DO
        
        # Perform the tasks
        for s in taskImport.values:
            if str(s)[0] == '(':
                vbdeSARIMA.evaluateTuple(eval(s)) # Automatically updates vS, y and yhat
        
        # Exports partial results
        vbdeSARIMA.vS.to_csv('worker '+str(i)+' output vS.csv', 
                  index = True, header = True)
        vbdeSARIMA.y.to_csv('worker '+str(i)+' output y.csv', 
                  index = True, header = True)
        vbdeSARIMA.yhat.to_csv('worker '+str(i)+' output yhat.csv', 
                  index = True, header = True)
        
        return(1)
    except:
       return(0)


"""
    MAIN BODY TO TEST THE FUNCTIONS


if __name__ == "__main__":
    print("Starts the execution")
        
    #General parameters for plotting figures
    plt.style.use('seaborn-darkgrid')
    matplotlib.rcParams['axes.labelsize'] = 14
    matplotlib.rcParams['xtick.labelsize'] = 12
    matplotlib.rcParams['ytick.labelsize'] = 12
    matplotlib.rcParams['text.color'] = 'k'
    palette = plt.get_cmap('Set1')
    
    # Dates of the observation & prediction periods considered in the model
    to1 = np.datetime64('2019-04-01 00:00:00')
    to2 = np.datetime64('2019-06-16 23:59:59')
    tp1 = np.datetime64('2019-06-17 00:00:00')
    tp2 = np.datetime64('2019-06-30 23:59:59')
    
    # Demand Input for all the information
    DemandInput = pd.read_csv('yS.csv')
    DemandInput['pickup_datetime'] = pd.to_datetime(DemandInput['pickup_datetime'], errors='coerce')
    yS = DemandInput[DemandInput['pickup_datetime'] >= to1]
    yS = yS[yS['pickup_datetime'] <= to2]
    
    # Creates observation time index
    to = yS.index.values
    
    yP = DemandInput[DemandInput['pickup_datetime'] >= tp1]
    yP = yP[yP['pickup_datetime'] <= tp2]
    
    yS = yS.set_index('pickup_datetime')
    yP = yP.set_index('pickup_datetime')
    
    # Test function is the aggregate demand in the control period
    yTest = yP['n']
    
    # Set of companies
    c = list(yS.columns.values)
    try:
        c.remove('n')
    except:
        pass

    companies = pd.Series(c)
    yS.columns = ['n'] + list(companies.index)
    
    # Interpolates linearly between ibservations when needed (nan)
    yS.interpolate(inplace = True, method = 'linear')
    yS.fillna(inplace = True, method = 'bfill')
    yS.fillna(0, inplace = True)
    
    #Creates the cluster and the client
    #config.daskClient = Client()
    #print("Dask client created: " + str(config.daskClient))
    
    print("Initialize model")
    #  Initializes the infrastructure needed: valuation metric, yTest, and 
    # the rest of parameters required to run the model (it performs a
    #grid search analysis to test and feed the best params)
    initializeModel(yS, yTest, nd = 14, maxiter = 200, depth = 2)
    
    # You can work with the model:    
    yPred = getModelOutput(yS['n'])
    
    # Plots the prediction
    plt.figure(figsize=(28,16))
    yPred.plot(kind='line', color = palette(2))
    yTest.plot(kind='line', color = palette(3))
    yS['n'].plot(kind='line', color = palette(4))
    plt.suptitle('Model fit with all the information', size = 'xx-large')
    plt.show()
    
    # Calculates cossim
    vN = calculateCosSim(yPred)
    print('Cosine similarity: ' + str(vN))
    print('Numerical similarity: ' + str(calculateNumSim(yPred)))
    print('Correlation: ' + str(calculateCrossCorrelation(yPred)))
    print('Relative SSE (-): ' + str(calculateRelativeSSE(yPred)))

    # Prepares the whole system to work together
    fw.initialize(config.daskClient, getCombinedInput, getModelOutput, calculateCosSim)
    
    # Calculates individual values
    print ("Individual values for the sources: ")
    print (str(fw.getIndividualValues(tuple(range(len(c))))))
    
    print ("LOO values for the sources: ")
    print (str(fw.getLOOValues(tuple(range(len(c))))))
    
   
    print ("Shapley values: ")
    (SV, dfP, nExecs) = fw.getSV_TSS(tuple(range(len(c))), 4, vN*0.95)
    print(str(SV))

  
    client.close()
"""
    